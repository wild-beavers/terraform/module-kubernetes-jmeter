# Default example

## Usage

```
# terraform init
# terraform apply
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| random | n/a |

## Inputs

No input.

## Outputs

| Name | Description |
|------|-------------|
| this | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
